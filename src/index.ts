import * as cheerio from 'cheerio'
import axios from 'axios'
import * as moment from 'moment-timezone'

class UBScraper {
  private $: CheerioStatic

  public async load(): Promise<this> {
    // Load fund index page

    const res = await axios.get('https://unitedbankers.fi/palvelut-ja-tuotteet/rahastot/rahastojen-arvot/#!')
    this.$ = cheerio.load(res.data)
    return this
  }

  public async index() {
    const ret = {}

    // parse fund ISIN codes and names
    this.$('.graph-toggle').each((i, el) => {
      const { attribs: { 'data-name': name, 'data-isin': ISIN } } = el
      ret[name] = ISIN
    })

    return ret
  }

  public async getValues(ISIN: string) {
    // GET values of fund based on ISIN

    const res = await axios.get(`https://unitedbankers.fi/json/fundValues/${ISIN}.json`)
    const data = res.data

    return data.map((el, i) => {
      const date = moment.tz(el[0], 'UTC').toISOString()
      const nav = el[1]

      return [date, nav]
    })
  }
}

const r = async () => {
  // initialise scraper
  const scraper = await new UBScraper().load()  

  // get fund names and ISIN codes
  const index = await scraper.index()

  // Get UB amerikka A values
  let values = await scraper.getValues(index['UB Amerikka A'])

  // get closing values
  values = values.filter((el, i) => {
    try {
      const today = moment(el[0])
      const tomorrow = moment(values[i + 1][0])
      const isTodayLastDayOfMonth = today.month() !== tomorrow.month()
      return isTodayLastDayOfMonth
    } catch {
      // it's ok to fail
    }
  })

  // console log in prettier format
  values.forEach(element => {
    console.log(element[0], element[1])
  })  
}

r()